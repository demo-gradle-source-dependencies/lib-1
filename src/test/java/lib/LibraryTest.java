package lib;

import org.junit.Test;
import static org.junit.Assert.*;

public class LibraryTest {
    @Test 
    public void testGetVersion() {
        Library lib = new Library();
        assertEquals(
            "getVersion should return '1.0.0'", lib.getVersion(), "1.0.0"
        );
    }
}
